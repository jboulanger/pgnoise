 #
 # Copyright Jérôme Boulanger (2019)
 #

 #make CROSS=x86_64-w64-mingw32.static- dll

PKG = pgnoise
VER = 1.0

CXX=$(CROSS)g++
CC=$(CROSS)gcc
LD=$(CROSS)ld
AR=$(CROSS)ar
PKG_CONFIG=$(CROSS)pkg-config

CXXFLAGS = -std=c++11  -I./ -fopenmp -pthread -Wall -W -pedantic -L./ -Dcimg_strict_warnings -Dcimg_use_openmp -Dcimg_display=0

# Program
EXE = test_pgnoise

# common libs
LIBS =

test_pgnoise_SRCS = test_pgnoise.cpp
test_pgnoise_LIBS = $(LIBS) pgnoise

# Libraries (libxxx.a)
LIB = libpgnoise.a

libpgnoise.a_SRCS = pgnoise.cpp
libpgnoise.a_LIBS =



include magick.mk

doc:
	doxygen doxyfile.cfg
	mkdir -p doc
	cd doc/latex; make
	mv doc/latex/refman.pdf ./pgnoise-1.0.pdf

# Simple target for distribution of sources
dist:
	mkdir -p $(PKG)-$(VER)
	cp *.cpp *.h Makefile magick.mk $(PKG)-$(VER)/
	tar zcf $(PKG)-$(VER).tgz $(PKG)-$(VER)
	rm -rf $(PKG)-$(VER)

dll:
	$(CXX) -std=c++11 -Wall -W -pedantic -Dcimg_display=0 -DDLL_BUILD -Ofast -c pgnoise.cpp -o pgnoise.o -fopenmp
	$(CXX) -std=c++11 -Wall -W -pedantic -Dcimg_display=0 -DDLL_BUILD -s -shared pgnoise.o -o pgnoise.dll -Wl,--output-def,tmp.def -fopenmp
	echo "LIBRARY LIBNDSAFIR" | cat - tmp.def > pgnoise.def
	rm -f tmp.def
	$(CXX) -std=c++11 -Wall -W -pedantic -Dcimg_display=0  test_pgnoise.cpp -o test_pgnoise.exe pgnoise.dll -mwindows -mconsole  -fopenmp
