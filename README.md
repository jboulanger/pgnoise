# Poisson-Gaussian noise analysis

Small c library to estimate and simulate Poisson-Gaussian noise.

The code is based on the c++ CImg library.

The estimation procedure is based on an analysis of the noise variance versus the expected mean following https://doi.org/10.1109/TMI.2009.2033991
.

Jerome Boulanger
