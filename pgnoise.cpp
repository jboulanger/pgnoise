/**
 * \file pgnoise.cpp
 * \brief Poisson-Gaussian noise estimation and simulation
 * \author Jerome Boulanger (2019)
 */
#include "pgnoise.h"

#include "CImg.h"
using namespace cimg_library;

// robust linear regressionv y = a + b x
void robust_linear_regression(const CImg<> &x, const CImg<> & y,
			      double & a, double & b,
			      const unsigned int max_iter=100, const double tol=1e-6) {
  if (x.is_empty()) throw CImgException("linear_fit(x,y,a,b): x is empty");
  if (y.is_empty()) throw CImgException("linear_fit(x,y,a,b): y is empty");
  if (x.size() != y.size()) throw CImgException("linear_fit(): 1st parameter x (%d,%d,%d,%d) and"
						"second parameter y (%d,%d,%d,%d) have not the same size",
						x.width(),x.height(),x.depth(),x.depth(),
						y.width(),y.height(),y.depth(),y.depth());
  double sw = 0, swxx = 0, swx = 0,swy = 0, swxy = 0;
  // init least square
  const float *ptr_xi = x.data(), *ptr_yi = y.data();
  for (unsigned int i = 0; i < x.size(); ++i) {
    const float xi = *ptr_xi++, yi = *ptr_yi++;
    sw++;
    swx += xi;     swy += yi;
    swxx += xi*xi; swxy += xi*yi;
  }
  double d = sw * swxx - swx * swx;
  a = (swxx * swy - swx * swxy) / d;
  b = (sw * swxy - swx * swy) / d;
  if (max_iter > 1) {
    // compute variance of residuals
    double sigma = 0, mean = 0, nsamples = 0, r_new = 0, r_old = 0;
    ptr_xi = x.data(); ptr_yi = y.data();
    for (unsigned int i = 0; i < x.size(); ++i) {
      const double xi = *ptr_xi++, yi = *ptr_yi++, delta = yi-(a+b*xi);
      mean += delta; sigma += delta*delta; nsamples++;
    }
    mean /= nsamples; sigma /= nsamples; r_new=sigma; sigma -= mean*mean;
    // IRLS
    unsigned int k = 0;
    while ( k<max_iter && (k<5 || cimg::abs((r_old-r_new)/r_old) > tol) )  {
      ptr_xi=x.data(); ptr_yi=y.data();
      r_old=r_new; r_new=0; sw=0; swxx=0; swx=0; swy=0; swxy=0;
      for (unsigned int i = 0; i < x.size(); ++i){
	const double xi = *ptr_xi++, yi = *ptr_yi++,
	  delta = yi - (a+b*xi), w = std::exp(-0.5*delta*delta/(9.0*sigma));
	if (!std::isnan(w)){
	  sw += w;
	  swx += w*xi;     swy += w*yi;
	  swxx += w*xi*xi; swxy += w*xi*yi;
	  r_new += delta*delta;
	}
      }
      d = sw * swxx - swx * swx;
      r_new /= nsamples;
      a = (swxx * swy - swx * swxy) / d;
      b = (sw * swxy - swx * swy) / d;
      ++k;
    }
  }
}

// normalized pseudo residuals to that Var[I+eps] = Var[eps] with I smooth
CImg<> pseudoresiduals(const CImg<> & img) {
  CImg<> residuals(img.width(), img.height(), img.depth(), 1);
  if (img.depth() > 1) {
    const float cste = 1.0 / std::sqrt(42.0);
    CImg_3x3x3(I,float);
    cimg_for3x3x3(img,x,y,z,0,I,float) {
      residuals(x,y,z) = cste * (Incc + Ipcc + Icnc + Icpc + Iccn + Iccp - 6*Iccc);
    }
  } else {
    const float cste = 1.0 / std::sqrt(20.0);
    CImg_3x3(I,float);
    cimg_for3x3(img,x,y,0,0,I,float) {
      residuals(x,y) = cste * (Inc + Ipc + Icn + Icp - 4*Icc);
    }
  }
  return residuals;
}

extern "C"
{

  DLL_EXPORT
  estimate_pg_noise(double * ptr_g0, double * ptr_edc,
		    double * ptr_m, double * ptr_sigma0,
		    const unsigned short * ptr_src,
		    const int width, const int height, const int depth) {
    try {
      const int n = 7;
      CImg<> img = CImg<unsigned short>(ptr_src,width,height,depth);
      CImg<> E = img.get_blur_median(n);
      CImg<> V = pseudoresiduals(img);
      V = (1.4826 * (V-V.get_blur_median(n)).abs().blur_median(n)).sqr();
      double edc0,g0;
      robust_linear_regression(E,V,edc0,g0);
      float m = E.min(), sigma0 = std::sqrt(V.min());
#ifdef DEBUG
      float edc =  sigma0*sigma0-g0*m
      printf("g0=%.2f,m=%.2f,sigma0=%.2f,edc=%.2f/%.2f\n",g0,m,sigma0,edc0,edc);
#endif
      *ptr_g0 = g0;
      *ptr_edc = edc0;
      *ptr_sigma0 = sigma0;
      *ptr_m = m;
    } catch (CImgException e) {
      return 1;
    }
    return 0;
  }

  DLL_EXPORT
  generate_pg_noise(unsigned short * ptr_dest,
		    const unsigned short * ptr_src,
		    const int width, const int height, const int depth,
		    const double g0,const double g1, const double m, const double sigma0) {
    try {
      const size_t n = width * height * depth;
      for (size_t i = 0; i < n; ++i) {
	const double val = g0 * cimg::prand(std::max(0.0,(*ptr_src++ - m) / g1)) + sigma0 * cimg::grand() + m;
	*ptr_dest++ = (unsigned short)std::max(0.0,std::min(65535.0,val));
      }
    } catch (CImgException e) {
      return 1;
    }
    return 0;
  }
}
