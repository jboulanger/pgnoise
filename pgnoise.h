/**
 * \file pgnoise.h
 * \brief Poisson-Gaussian noise estimation and simulation
 * \author Jerome Boulanger (2019)
 */
#pragma once
#ifdef DLL_BUILD
#define DLL_EXPORT __declspec (dllexport) int __cdecl
#else
#define DLL_EXPORT int
#endif

extern "C"
{
  //! Estimate Poisson-Gaussian noise parameters
  /**
     \param[out] ptr_g0 Estimated gain
     \param[out] ptr_edc Estimated noise offset
     \param[out] ptr_m Estimated signal baseline
     \param[out] ptr_sigma0 Estimated standard deviation of the additive Gaussian noise
     \param ptr_src Pointer to the memory buffer containing the image
     \param width Width of the imag
     \param height Height of the image
     \param depth Depth of the image
  */

  DLL_EXPORT
  estimate_pg_noise(double * ptr_g0, double * ptr_edc,
		    double * ptr_m, double * ptr_sigma0,
		    const unsigned short * ptr_src,
		    const int width, const int height, const int depth);

  //! Generate Poisson-Gaussian noise
  /**
     \param[out] ptr_dest Pointer to the memory buffer containint the noisy image
     \param ptr_src Pointer to the memory buffer containing the image
     \param width Width of the imag
     \param height Height of the image
     \param depth Depth of the image
     \param g0 Sensor gain
     \param g1 Scaling gain
     \param m Signal baseline
     \param sigma0 Standard deviation of the additive Gaussian noise

     \note The output image \f$y\f$ is generated from the input image
     \f$x\f$ at each pixel i as \f$ y_i = g_0 N_i + \varepsilon_i\f$
     where the number of photons \f$N_i\f$ follows a Poisson
     distribution \f$ N_i \sim \mathrm{Poisson}(\frac{x-m}{g_1})\f$
     and the readout noise \f$\varepsilon_i\f$ follows a Gaussian
     distribution \f$ \varepsilon_i \sim
     \mathrm{Normal}(m,\sigma_0^2)\f$
  */
  DLL_EXPORT
  generate_pg_noise(unsigned short * ptr_dest,
		    const unsigned short * ptr_src,
		    const int width, const int height, const int depth,
		    const double g0,const double g1, const double m, const double sigma0);
}
