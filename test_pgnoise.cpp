#include "CImg.h"

using namespace cimg_library;

#include "pgnoise.h"


int main(/*int argc, char* argv[]*/) {
  int width = 256, height = 256, depth = 16;
  CImg<> img0(width, height, depth);
  img0.noise(1).blur(10).normalize(-10,1000).max(0.0f);
  CImg<unsigned short> img(img0), noisy(width, height, depth);
  double g0 = 1+2*cimg::rand(), g1 = 1+2*cimg::rand(), m = 10+40*cimg::rand(), sigma = 1+9*cimg::rand();
  if (generate_pg_noise(noisy.data(), img.data(), width, height, depth, g0, g1, m, sigma)==1) {
    printf("error in estimate_pg_noise");
  }
  double g0_est = 0, edc_est, m_est = 0, sigma_est = 0;
  cimg::tic();
  if(estimate_pg_noise(&g0_est, &edc_est, &m_est, &sigma_est, noisy.data(), width, height, depth)==1) {
    printf("error in estimate_pg_noise");
  }
  printf("\ttrue\testim.\terror (%%)\n"
	 "gain\t%.2f\t%.2f\t%.2f\n"
	 "baseli.\t%.2f\t%.2f\t%.2f\n"
	 "awgn\t%.2f\t%.2f\t%.2f\n",
	 g0,g0_est,std::abs(g0-g0_est)/g0*100.,
	 m,m_est,std::abs(m-m_est)/m*100.,
	 sigma,sigma_est,std::abs(sigma-sigma_est)/sigma_est*100.0);
  cimg::toc();
  return 0;
}
